<?php

namespace Drupal\migrate_default_content\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Migrate Default Content settings for this site.
 *
 * @todo Add validation to sure the directory set in source_dir actually exists.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'migrate_default_content_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['migrate_default_content.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['source_dir'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Source directory'),
      '#description' => $this->t('The directory where the default content is stored.'),
      '#default_value' => $this->config('migrate_default_content.settings')->get('source_dir'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('migrate_default_content.settings')
      ->set('source_dir', $form_state->getValue('source_dir'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}

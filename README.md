# migrate_default_content

Migrate default content provides a way to import content into a site using yaml files stored in the `default_content` directory of a drupal project.

## Configuring the default directory

By default, the content directory is located at the root of a project in the `default_content` directory. You can customize this by changing the `source_dir` value in the config.

## File format

The files used to import content should be placed inside the default directory and match the following naming scheme: `ENTITY_TYPE.BUNDLE.yml`.

For example: if you wanted to import default article nodes, you would put them in `node.article.yml`. Following the same logic, if you wanted to have default users you would put them in `user.user.yml`.

## Entity references

Entity reference fields will attempt to add dependencies automatically from other files present in the `default_content` directory.

For example: if you have a user migration and you specify the author of a node, the user will be looked up in the user migration file when creating the node.

```yml
# Directory structure:
# default_content
# |── user.user.yml
# └── node.article.yml

# user.user.yml
-
  name: Ed I. Tor

# node.article.yml
-
  title: Example article
  body: This is the body text for the article
  uid: Ed I. Tor
```

Entity reference revision fields behave roughly the same.

### Custom identifiers

The first column (or element) of any file will be used as the identifier for that migration.

For example: if you want to specify user UUIDs and reference those instead of a user's name.

```yml
# Directory structure:
# default_content
# |── user.user.yml
# └── node.article.yml

# user.user.yml
-
  uuid: 3ef9f089-dad2-4878-9da4-a12a56b568b5
  name: Ed I. Tor

# node.article.yml
-
  title: Example article
  body: This is the body text for the article
  uid: 3ef9f089-dad2-4878-9da4-a12a56b568b5
```

## Migrating files

Files can be migrated automatically by placing them in the `files` directory inside the `default_content` directory.

The files will be migrated to the `public://` stream wrapper by default.

```yml
# Directory structure:
# default_content
# |── files
# |   └── example-image.jpg
# └── node.article.yml

# node.article.yml
-
  title: Example article
  body: This is the body text for the article
  field_image: example-image.jpg
```

### Custom file migrations

You can also create your own files migration in case you need more fields like uuid or the source of the image.

```yml
# Directory structure:
# default_content
# |── files
# |   └── example-image.jpg
# |── file.file.yml
# └── node.article.yml

# file.file.yml
-
  uuid: 63299a78-436c-4a9e-bc32-79541f2cd6ad
  filename: example-image.jpg
  field_source: https://example.com/example-image.jpg

# node.article.yml
-
  title: Example article
  body: This is the body text for the article
  field_image: 63299a78-436c-4a9e-bc32-79541f2cd6ad
```

## Migrating menu link content

Menu links can be migrated with a `menu_link_content.menu_link_content.yml` file.

```yml
# Directory structure:
# default_content
# └── menu_link_content.menu_link_content.yml

# menu_link_content.menu_link_content.yml
-
  title: Internal Link
  link: internal:/internal-link
  menu_name: main
  weight: 0
-
  title: External Link
  link: https://example.com
  menu_name: main
  weight: 1
```

## Multi-component fields

Some fields such as formatted text or text with summary have multiple components to the field (e.g. value, format, and summary). You can map them in your migration file in two different ways.

### Camel case subcomponents

You can separate the components of the field by capitalizing the subcomponents using camel case.

```yml
# Directory structure:
# default_content
# └── node.article.yml

# node.article.yml
-
  title: Example article
  bodyValue: This is the body text for the article
  bodyFormat: basic_html
  bodySummary: This is the summary text for the article
```

### Nested fields

You can also nest the components of the field under the field name.

```yml
# Directory structure:
# default_content
# └── node.article.yml

# node.article.yml
-
  title: Example article
  body:
    value: This is the body text for the article
    format: basic_html
    summary: This is the summary text for the article
```

## Translation support

You can import translated content by placing the translated files in the `default_content` directory with the following naming scheme: `ENTITY_TYPE.BUNDLE.LANGCODE.yml`.

For example: if you wanted to import a Spanish translation of an article node, you would put it in `node.article.es.yml`.

You also must define a `translation_origin` field that references the key of the original content.

```yml
# Directory structure:
# default_content
# |── node.article.yml
# └── node.article.es.yml

# node.article.yml
-
  title: Example article
  body: This is the body text for the article

# node.article.es.yml
-
  title: Artículo de ejemplo
  body: Este es el texto del cuerpo del artículo
  translation_origin: Example article
```

## Password handling

Any field with the password data type will be hashed automatically.

## Migration information

All migrations will be tagged with `migrate_default_content` and will be grouped by the entity type, allowing you to specify which group of migrations to run.

For more information on running migrations, see the [Migrate API documentation](https://www.drupal.org/docs/drupal-apis/migrate-api/executing-migrations).

## Examples

You can find examples of migrations in the [examples](https://git.drupalcode.org/project/migrate_default_content/-/tree/8.x-2.x/examples) directory.
